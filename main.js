function generateFlashcard() {
  fetch('https://filipstal.pythonanywhere.com/randomqa')
    .then(response => response.json())
    .then(data => {
      document.getElementById('question').textContent = data.question;
      document.getElementById('answer').textContent = data.answer;
      document.getElementById('formatted').textContent = data.formatted;
    })
    .catch(error => console.error('Error:', error));
}
